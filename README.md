# Hugo Theme: Doc *et al*

Doc *et al* or *Docetal*, for Documentation and other tools, is theme for [Hugo](https://gohugo.io). It's designed to present a project with frontpages, documentation, offline usage and PDF export.

It was inspired by the [Dgraph](https://dgraph.io/docs), [Syna](https://about.okkur.org/syna), [Grav Learn](https://learn.getgrav.org) and [Learn](https://learn.netlify.com) themes.

Doc *et al* uses modern Hugo features:
* [Assets](https://gohugo.io/categories/asset-management) to manage CSS and Javascript files (minifying and versioning)
* [Markup](https://gohugo.io/getting-started/configuration-markup) to render links to images

## Features

* Layouts for front (default) and documentation (*doc*) pages
* Layout for *listing* files within a directory
* Concatenate all documentation into single page for printing or PDF export (*agg* layout)
* Unlimited menu levels and Table of contents in documentation
* List child pages (*children* layout)
* Search based on [Lunr](https://lunrjs.com)
* Customizable colors using themes variants
* [Font Awesome](https://fontawesome.com) for icons
* Offline. All resources (fonts, CSS, Javascript) are loaded locally.

## Download

See [refs](../../refs) page.

## Installation

Visit this [guide](https://github.com/vjeantet/hugo-theme-docdock) to install Doc *et al*.

## License

Doc *et al* is distributed under the Mozilla Public License Version 2.0 (see /LICENSE).
