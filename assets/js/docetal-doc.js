/*
 Doc et al.

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at https://www.mozilla.org/MPL/2.0/.

 Copyright (C) 2019-2022 Charles E. Vejnar
*/

function initContextbar() {
    let control = document.getElementById('contextbar-control')
    let bar = document.getElementById('contextbar')
    if (bar !== null) {
        control.state = 'on'
        control.onclick = function (e) {
            if (this.state == 'on') {
                this.state = 'off'
                this.classList.add('hidden')
                bar.style.display = 'none'
            } else {
                this.state = 'on'
                this.classList.remove('hidden')
                bar.style.display = ''
            }
        }
    }
}

addLoadEvent(initContextbar)

function initClipboardJS() {
    let pres = document.getElementsByTagName('PRE')
    for (let i=0, leni=pres.length; i<leni; i++) {
        let codes = pres[i].getElementsByTagName('CODE')
        for (let j=0, lenj=codes.length; j<lenj; j++) {
            if (codes[j].textContent.length > 6) {
                // Add Copy button
                let span = document.createElement('SPAN')
                span.className = 'copy-btn'
                span.textContent = 'COPY'
                span.targetCode = codes[j]
                pres[i].appendChild(span)
            }
        }
    }

    let clipboard = new ClipboardJS('.copy-btn', {
        text: function(trigger) {
            return trigger.targetCode.textContent
        }
    })

    clipboard.on('success', function(e) {
        e.clearSelection()
        e.trigger.textContent = 'COPIED TO CLIPBOARD!'

        window.setTimeout(function() {
            e.trigger.textContent = 'COPY'
        }, 2000)
    })

    clipboard.on('error', function(e) {
        e.clearSelection()
        e.trigger.textContent = 'ERROR COPYING'

        window.setTimeout(function() {
            e.trigger.textContent = 'COPY'
        }, 2000)
    })
}

addLoadEvent(initClipboardJS)

function initMenuHighlighter() {
    let observer = new IntersectionObserver(function(entries) {
        // Refresh anchor status
        for (let entry of entries) {
            if(entry.isIntersecting === true & entry.intersectionRatio === 1) {
                this.anchorsStatus.set(entry.target.anchorID, true)
            } else {
                this.anchorsStatus.set(entry.target.anchorID, false)
            }
        }

        // Find the active anchor
        let activeID = -1
        for (let [k, v] of this.anchorsStatus) {
            if (activeID === -1 & v === true) {
                activeID = k
                break
            }
        }

        // If new active anchor is found, activate it
        if (activeID !== -1) {
            for (let [k, v] of this.anchorsStatus) {
                if (this.anchors.get(k).menuEntry !== null) {
                    if (activeID === k) {
                        this.anchors.get(k).menuEntry.classList.add('active')
                    } else {
                        this.anchors.get(k).menuEntry.classList.remove('active')
                    }
                }
            }
        }
    }, { threshold: [0, 1] })

    let anchors = document.getElementById('main-text').getElementsByClassName('anchor')
    // Find the ToC in contextbar first
    let menu = document.getElementById('contextbar-toc')
    // Then find it in the sidebar
    if (menu === null) {
        menu = document.querySelector('li.main-topic.active')
    }
    if (menu !== null) {
        observer.anchors = new Map()
        observer.anchorsStatus = new Map()
        for (let i=0, leni=anchors.length; i<leni; i++) {
            anchors[i].menuEntry = menu.querySelector(`a[href='${anchors[i].hash}']`)
            anchors[i].anchorID = i
            observer.observe(anchors[i])

            observer.anchors.set(i, anchors[i])
            observer.anchorsStatus.set(i, false)
        }
    }
}

addLoadEvent(initMenuHighlighter)

function applyMaxHeight(currentNode, chevron) {
    let cumHeight = currentNode.scrollHeight
    currentNode.style.maxHeight = cumHeight + "px"
    // Set maxHeight on parent(s)
    let parent = chevron.parentNode
    while (parent != null && parent.id != 'sidebar' && parent.id != 'top-navbar-topics') {
        if (parent.nodeName == 'UL') {
            let level = parseInt(parent.getAttribute('data-nav-level'))
            if (level > 0 && parent.classList.contains('hidden') == false) {
                cumHeight += parent.scrollHeight
                parent.style.maxHeight = cumHeight + "px"
            }
        }
        parent = parent.parentNode
    }
}

function initTopicToggler() {
    let chevrons = document.getElementsByClassName('chevron')
    for (let i=0, leni=chevrons.length; i<leni; i++) {
        let children = chevrons[i].parentNode.parentNode.childNodes
        for (let j=0, lenj=children.length; j<lenj; j++) {
            if (children[j].nodeName == 'UL') {
                // Set maxHeight
                if (children[j].classList.contains('hidden')) {
                    children[j].style.maxHeight = 0
                } else {
                    applyMaxHeight(children[j], chevrons[i])
                }
                // Set onclick on chevron
                chevrons[i].menu = children[j]
                chevrons[i].onclick = function (e) {
                    if (this.menu.classList.contains('hidden')) {
                        applyMaxHeight(this.menu, this)
                    } else {
                        this.menu.style.maxHeight = 0
                    }
                    this.classList.toggle('hidden')
                    this.menu.classList.toggle('hidden')
                }
                break
            }
        }
    }
}

addLoadEvent(initTopicToggler)

function toggleNavbar(node) {
    if (document.getElementById(node).classList.toggle('hidden')) {
        document.getElementById('main').style.overflowY = 'auto'
        document.getElementById('main-aside').style.overflow = 'auto'
    } else {
        document.getElementById('main').style.overflowY = 'visible'
        document.getElementById('main-aside').style.overflow = 'visible'
        initTopicToggler()
    }
}
