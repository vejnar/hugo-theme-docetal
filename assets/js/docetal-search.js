/*
 Doc et al.

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at https://www.mozilla.org/MPL/2.0/.

 Copyright (C) 2019-2022 Charles E. Vejnar
*/

class Search {
    constructor(domSearchInput, domSearchResults) {
        this.domSearchInput = domSearchInput
        this.domSearchResults = domSearchResults
        this.documents = []
        this.idx = null
    }

    initDocuments(urlData) {
        // Request and Index documents
        fetch(urlData, {
            method: 'get'
        }).then(
            res => res.json()
        ).then(
            res => {
                // Index document
                let documents = this.documents
                this.idx = lunr(function() {
                    this.ref('url')
                    this.field('title', {boost: 5})
                    this.field('content')

                    // Remove stemmer
                    this.pipeline.remove(lunr.stemmer)
                    this.searchPipeline.remove(lunr.stemmer)

                    res.forEach(function(doc) {
                        this.add(doc)
                        documents[doc.url] = {
                            'title': doc.title,
                            'content': doc.content,
                        }
                    }, this)
                })
                // Register handler
                this.registerSearchHandler()
            }
        ).catch(
            err => {
                console.log(`Error initializing search: ${err}`)
            }
        )
    }

    removeResults() {
        while (this.domSearchResults.firstChild) {
            this.domSearchResults.removeChild(this.domSearchResults.firstChild)
        }
    }

    registerSearchHandler() {
        // Input event
        this.domSearchInput.Search = this
        this.domSearchInput.oninput = function(event) {
            // Remove search results if the user empties the search input field
            if (this.Search.domSearchInput.value == '') {
                this.Search.removeResults()
                event.target.classList.remove('active')
            } else {
                // Input value
                let query = event.target.value
                // Run fuzzy search
                let results = this.Search.idx.search(query+'*')
                // Render results
                if (results.length > 0) {
                    event.target.classList.add('active')
                    this.Search.renderSearchResults(results)
                } else {
                    this.Search.removeResults()
                }
            }
        }
        // Display search results when search gains focus
        this.domSearchInput.onfocus = function(event) {
            if (this.Search.domSearchResults.firstChild) {
                event.target.classList.add('active')
            }
            this.Search.domSearchResults.style.visibility = ''
        }
        // Remove search results when search loses focus
        this.domSearchInput.onblur = function(event) {
            this.Search.domSearchResults.style.visibility = 'hidden'
            event.target.classList.remove('active')
        }
        // To avoid onblur to hide onclick
        this.domSearchResults.onmousedown = function(event) {
            event.preventDefault()
        }
    }

    renderSearchResults(results) {
        // Truncate results to 10
        if (results.length > 9){
            results = results.slice(0,10)
        }
        // Reset search results
        this.removeResults()
        // Append results
        results.forEach(result => {
            // Add result item
            let article = document.createElement('LI')
            let link = document.createElement('A')
            link.href = result.ref
            link.textContent = this.documents[result.ref].title
            article.appendChild(link)
            this.domSearchResults.appendChild(article)
        })
    }
}

function initSearch() {
    window.search = new Search(document.getElementById('search-input'), document.getElementById('search-results'))
    window.search.initDocuments(baseURL+'/index.json')
}

addLoadEvent(initSearch)
